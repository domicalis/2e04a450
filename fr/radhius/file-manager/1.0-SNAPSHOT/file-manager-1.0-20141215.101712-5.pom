<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>fr.radhius</groupId>
    <artifactId>file-manager</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>file-manager</name>

    <scm>
        <connection>scm:git@bitbucket.org:radhius/file-manager.git</connection>
        <developerConnection>scm:git@bitbucket.org:radhius/file-manager.git</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jersey.version>1.9</jersey.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-client</artifactId>
            <version>${jersey.version}</version>
        </dependency>

        <dependency>
            <groupId>com.sun.jersey.contribs</groupId>
            <artifactId>jersey-multipart</artifactId>
            <version>${jersey.version}</version>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>18.0</version>
        </dependency>
        <!--  Gson: Java to Json conversion -->
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.4</version>
        </dependency>

        <!-- logger -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.6.1</version>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.0.13</version>
            <scope>test</scope>
        </dependency>

        <!-- TESTS -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.easytesting</groupId>
            <artifactId>fest-assert</artifactId>
            <version>1.4</version>
            <scope>test</scope>
        </dependency>

        <!-- Findbugs -->
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>annotations</artifactId>
            <version>2.0.0</version>
        </dependency>

    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.5.1</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <reportPlugins>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-project-info-reports-plugin</artifactId>
                            <version>2.4</version>
                            <configuration>
                                <dependencyDetailsEnabled>false</dependencyDetailsEnabled>
                                <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
                            </configuration>
                            <reports>
                                <report>dependencies</report>
                            </reports>
                        </plugin>
                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>findbugs-maven-plugin</artifactId>
                            <version>2.5.5</version>
                            <configuration>
                                <excludeFilterFile>config/findbugs/exclude.xml</excludeFilterFile>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-pmd-plugin</artifactId>
                            <version>3.2</version>
                            <reportSets>
                                <reportSet>
                                    <reports>
                                        <report>pmd</report>
                                    </reports>
                                </reportSet>
                            </reportSets>
                            <configuration>
                                <excludeRoots>
                                    <excludeRoot>target/generated-sources/cxf</excludeRoot>
                                </excludeRoots>
                                <printFailingErrors>true</printFailingErrors>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-checkstyle-plugin</artifactId>
                            <version>2.10</version>
                            <configuration>
                                <configLocation>config/checkstyle/checkstyle.xml</configLocation>
                                <suppressionsLocation>config/checkstyle/suppressions.xml</suppressionsLocation>
                                <suppressionsFileExpression>checkstyle.suppressions.file</suppressionsFileExpression>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-javadoc-plugin</artifactId>
                            <version>2.9.1</version>
                        </plugin>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-surefire-report-plugin</artifactId>
                            <version>2.4.3</version>
                        </plugin>
                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>cobertura-maven-plugin</artifactId>
                            <version>2.6</version>
                            <configuration>
                                <formats>
                                    <format>html</format>
                                    <format>xml</format>
                                </formats>
                            </configuration>
                        </plugin>
                    </reportPlugins>
                </configuration>
            </plugin>
        </plugins>
        <extensions>
            <extension>
                <groupId>ar.com.synergian</groupId>
                <artifactId>wagon-git</artifactId>
                <version>0.2.3</version>
            </extension>
        </extensions>
    </build>
    <pluginRepositories>
        <pluginRepository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
        </pluginRepository>
    </pluginRepositories>
    <distributionManagement>
        <repository>
            <id>radhius-release-bitbucket</id>
            <name>Radhius Bitbucket Repository - Release</name>
            <url>git:releases://git@bitbucket.org:radhius/2e04a450.git</url>
        </repository>
        <snapshotRepository>
            <id>radhius-snapshot-bitbucket</id>
            <name>Radhius Bitbucket Repository - Snapshot</name>
            <url>git:snapshots://git@bitbucket.org:radhius/2e04a450.git</url>
        </snapshotRepository>
    </distributionManagement>
</project>
